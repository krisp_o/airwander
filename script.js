(function () {
    'use strict';

    angular.module('app', []);

    angular
        .module('app')
        .controller('MainController', MainController);


    function MainController() {
        var vm = this;
        vm.dropdown = [];
        vm.selectedTrip = "";
        vm.places = {};

        vm.OnChange = function(){
           
            if(vm.selectedTrip === 'round'){
                vm.places.i3 = vm.places.i1;
            }
        }


        activate();

        ////////////////

        function activate() {
            vm.places = {
                i1: '',
                i2: '',
                i3: '',
                i4: '',
            }

            vm.dropdown = [{
                name: 'One-Way',
                value: 'oneway'
            },
            {
                name: 'Round-Trip',
                value: 'round'
            },
            {
                name: 'Multi-City',
                value: 'multi'
            },
            {
                name: 'World-Tour',
                value: 'tour'
            }]
        }
    }
})();